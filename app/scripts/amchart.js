// Setting AmChart for year value
	var yearChart = AmCharts.makeChart( "year", {
	  "type": "gauge",
	  "theme": "light",
	  "axes": [ {
		"axisThickness": 1,
		"axisAlpha": 0.2,
		"tickAlpha": 0.2,
		"valueInterval": 20,
		"bands": [ {
		  "color": "#cc4748",
		  "endValue": -10,
		  "startValue": -100
		}, {
		  "color": "#fdd400",
		  "endValue": 10,
		  "startValue": -10
		}, {
		  "color": "#84b761",
		  "endValue": 100,
		  "innerRadius": "95%",
		  "startValue": 10
		} ],
		"bottomText": "0 km/h",
		"bottomTextYOffset": -20,
		"endValue": 100,
		"startValue": -100
	  } ],
	  "arrows": [ {} ],
	  "export": {
		"enabled": true
	  }
	} );

	setInterval( yearValue, 2000 );

	// set random value
	function yearValue() {
	  var value = 60;
	  if ( yearChart ) {
		if ( yearChart.arrows ) {
		  if ( yearChart.arrows[ 0 ] ) {
			if ( yearChart.arrows[ 0 ].setValue ) {
			  yearChart.arrows[ 0 ].setValue( value );
			  yearChart.axes[ 0 ].setBottomText( "Year, %" );
			}
		  }
		}
	  }
	}


	// setting AmChart for month value
	var monthChart = AmCharts.makeChart( "month", {
	  "type": "gauge",
	  "theme": "light",
	  "axes": [ {
		"axisThickness": 1,
		"axisAlpha": 0.2,
		"tickAlpha": 0.2,
		"valueInterval": 20,
		"bands": [ {
		  "color": "#cc4748",
		  "endValue": -10,
		  "startValue": -100
		}, {
		  "color": "#fdd400",
		  "endValue": 10,
		  "startValue": -10
		}, {
		  "color": "#84b761",
		  "endValue": 100,
		  "innerRadius": "95%",
		  "startValue": 10
		} ],
		"bottomText": "0 km/h",
		"bottomTextYOffset": -20,
		"endValue": 100,
		"startValue": -100
	  } ],
	  "arrows": [ {} ],
	  "export": {
		"enabled": true
	  }
	} );

	setInterval( monthValue, 2000 );

	// set random value
	function monthValue() {
	  var value = 50;
	  if ( monthChart ) {
		if ( monthChart.arrows ) {
		  if ( monthChart.arrows[ 0 ] ) {
			if ( monthChart.arrows[ 0 ].setValue ) {
			  monthChart.arrows[ 0 ].setValue( value );
			  monthChart.axes[ 0 ].setBottomText( "Month, %" );
			}
		  }
		}
	  }
	}
	// setting AmChart for week value
	var weekChart = AmCharts.makeChart( "week", {
	  "type": "gauge",
	  "theme": "light",
	  "axes": [ {
		"axisThickness": 1,
		"axisAlpha": 0.2,
		"tickAlpha": 0.2,
		"valueInterval": 20,
		"bands": [ {
		  "color": "#cc4748",
		  "endValue": -10,
		  "startValue": -100
		}, {
		  "color": "#fdd400",
		  "endValue": 10,
		  "startValue": -10
		}, {
		  "color": "#84b761",
		  "endValue": 100,
		  "innerRadius": "95%",
		  "startValue": 10
		} ],
		"bottomText": "0 km/h",
		"bottomTextYOffset": -20,
		"endValue": 100,
		"startValue": -100
	  } ],
	  "arrows": [ {} ],
	  "export": {
		"enabled": true
	  }
	} );

	setInterval( weekValue, 2000 );

	// set random value
	function weekValue() {
	  var value = 80;
	  if ( weekChart ) {
		if ( weekChart.arrows ) {
		  if ( weekChart.arrows[ 0 ] ) {
			if ( weekChart.arrows[ 0 ].setValue ) {
			  weekChart.arrows[ 0 ].setValue( value );
			  weekChart.axes[ 0 ].setBottomText( "Week, %" );
			}
		  }
		}
	  }
	}