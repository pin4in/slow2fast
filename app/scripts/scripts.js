$(document).ready(function() {

	var info = $('.info');
	var datepicker = $('.ft-datepicker');
	var modal = $('.modal');
	var login = $('.login');
	var register = $('.register');
	var toLogin = $('.go-to-login');
	var toRegister = $('.go-to-register');


	// INITIALIZE BOOTSTRAP MATERIAL BUILD IN FUNCTIONS
	$.material.init();

	// INIT OWL CAROUSEL ON THE FRONT PAGE
	$("#owl-frontPage").owlCarousel({
 
			navigation : true, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem:true,
			autoPlay : 3000,
			navigationText: ['<i class="mdi-navigation-arrow-back"></i>' , '<i class="mdi-navigation-arrow-forward"></i>']
	});

	// NORMALIZ PADDING OF INFOWINDOW ON THE PORTFOLIO PAGE
	if(info.length > 0) {
		var h = info.height();
		$('.pad-top-50').css('padding-top' , h - 30)
	}

	// TOGGLE TABLE TITLE WITH DATE PICKER ON THE ARCHIVE PAGE
	if(datepicker.length > 0) {
		datepicker.click(function() {
			$(this).addClass('ft-datepicker--show');
		});
	}

	toLogin.click(function() {
		login.show();
		register.hide();
	})

	toRegister.click(function() {
		login.hide();
		register.show();
	})
	
	
});